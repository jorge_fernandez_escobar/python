'''
Instrucciones por entrada de teclado:
'c': Captura de imagen.
'e': Habilita autoexposición.
'f': Habilita autofoco continuo.
't': Gatilla autofoco y deshabilita autofoco continuo.
'b': Habilita balance de blanco automático
'i', 'o': Tiempo de exposición de 1...200000 [us]
'k', 'l': Sensibilidad ISO de 100...3000
',', '.': Foco de 0...255 [lejos...cerca]
'{', '}': Balance de blanco en temperatura luz 1000...12000 [°K]


'''

import cv2
import depthai
import datetime
import time
import os
import sys
import threading


#########################################################################################
# VARIABLES
#########################################################################################

# Manual exposure/focus/white-balance set step
EXP_STEP = 500  # us
ISO_STEP = 50
LENS_STEP = 3
WB_STEP = 200

# Periodo de captura
T_captura = 600 # segundos

# Tiempo de captura inicial
T_captura_inicial = 30 # segundos

# Dirección IP de cámara
cam_ip_address = "10.16.44.52"

# Identificador de cámara
cam_id = "cam2"

# Log de errores  
errorLogName = 'errorLog.csv'
pathErrorLog = '\DataOUT_test'
completePathErrorLog = os.path.join(pathErrorLog, errorLogName)


#########################################################################################
# FUNCIONES
#########################################################################################

def tomar_captura():
    print("Tomar Captura Sincronica", datetime.datetime.now())
    ctrl = depthai.CameraControl()
    ctrl.setCaptureStill(True)
    controlQueue.send(ctrl)

def clamp(num, v0, v1):
    return max(v0, min(num, v1))


#########################################################################################
#
#########################################################################################

# Start defining a pipeline
pipeline = depthai.Pipeline()

# Mejora de cámara para poca luz
pipeline.setCameraTuningBlobPath('tuning_color_low_light.bin')

# Define a source - color camera
camRGB = pipeline.create(depthai.node.ColorCamera)
#xoutRGB = pipeline.create(depthai.node.XLinkOut)
#xoutRGB.setStreamName("video")

stillEncoder = pipeline.create(depthai.node.VideoEncoder)
outStill = pipeline.create(depthai.node.XLinkOut)
outStill.setStreamName("still")

controlIn = pipeline.create(depthai.node.XLinkIn)
controlIn.setStreamName("control")

# Properties
camRGB.setResolution(depthai.ColorCameraProperties.SensorResolution.THE_12_MP)
camRGB.setPreviewSize(800, 600)
camRGB.setInterleaved(False)
camRGB.setColorOrder(depthai.ColorCameraProperties.ColorOrder.RGB)

stillEncoder.setDefaultProfilePreset(1, depthai.VideoEncoderProperties.Profile.MJPEG)

# Linking
#camRGB.preview.link(xoutRGB.input)

camRGB.still.link(stillEncoder.input)
stillEncoder.bitstream.link(outStill.input)

controlIn.out.link(camRGB.inputControl)

# 
device_info = depthai.DeviceInfo()
device_info.state = depthai.XLinkDeviceState.X_LINK_BOOTLOADER
device_info.desc.protocol = depthai.XLinkProtocol.X_LINK_TCP_IP
device_info.desc.name = cam_ip_address


# Connect to device with pipeline
with depthai.Device(pipeline, device_info) as device:

    # 
    #qRGB = device.getOutputQueue(name="video", maxSize=4, blocking=False)

    #
    controlQueue = device.getInputQueue('control')

    # Defaults and limits for manual focus/exposure controls
    lensPos = 170
    lensMin = 0
    lensMax = 255

    expTime = 1000
    expMin = 1
    expMax = 33000

    sensIso = 100
    sensMin = 100
    sensMax = 3000
    
    wbManual = 5000
    wbMin = 1000
    wbMax = 12000

    # Tomar captura
    timer2 = threading.Timer(T_captura_inicial, tomar_captura)
    timer2.start()

    while True:
        #try:
        #    inRGB = qRGB.get()
        #except:
        #    print("Error qRGB.get()")#pass      
    
        try:
            stillQueue = device.getOutputQueue("still")
        except:
            print(cam_id, "Error device.getOutputQueue(still)")#pass

            # Escribir en log (append)
            #with open('%s' % errorLogName, 'a') as f:
            with open(completePathErrorLog, 'a') as f:
                print(datetime.datetime.now(), cam_id, "Error device.getOutputQueue(still)", sep=',',file=f)

            # Salir del script
            sys.exit()            

        
        try:
            controlQueue = device.getInputQueue("control")
        except:
            print(cam_id, "device.getOutputQueue(control)")#pass
        
            # Escribir en log (append)
            with open(completePathErrorLog, 'a') as f:
                print(datetime.datetime.now(), cam_id, "Error device.getOutputQueue(control)", sep=',',file=f) 

            # Salir del script
            sys.exit()  

        # Mostrar stream "video"
        #try:
        #    cv2.imshow("video", inRGB.getCvFrame())
        #except:
        #    print("cv2.imshow(video, inRGB.getCvFrame())")#pass

        
        # 
        try:
            stillFrames = stillQueue.tryGetAll()
        except:
            print(cam_id, "Error stillQueue.tryGetAll()")#pass

            # Escribir en log (append)
            with open(completePathErrorLog, 'a') as f:
                print(datetime.datetime.now(), cam_id, "Error stillQueue.tryGetAll()", sep=',',file=f)

            # Salir del script
            sys.exit()   
        
        for stillFrame in stillFrames:
            # Nombre Imagen
            ct = datetime.datetime.now()
            #Date = ct.date().strftime("%Y%m%d")
            #Time = ct.time().strftime("%H%M%S")
            Date = ct.date().strftime("%Y_%m_%d")
            Time = ct.time().strftime("%H_%M")
            imageName = cam_id + '_' + Date + '_' + Time + '.png'

            # Crear directorio
            path = cam_id
            try:
                os.mkdir(path)
            except OSError as error:
                pass#print(error)

            # Crear log  
            #logName = path + '/' + path + '_' + Date + '.csv'

            # Escribir en log (append)
            #with open('%s' % logName, 'a') as f:
            #    print(imageName, sep=',',file=f)
            #print(logName)
            
            # Decode JPEG
            frame = cv2.imdecode(stillFrame.getData(), cv2.IMREAD_UNCHANGED)
            
            imageName = path + '/' + imageName
            
            # Guardar captura
            cv2.imwrite(imageName, frame)

            # Cancelar timer de captura
            try:
                timer2.cancel()
                print("Terminar Timer de Captura")
            except:
                pass

            # Reagendar toma de captura 
            timer2 = threading.Timer(T_captura, tomar_captura)
            timer2.start()

    
        # Update screen (1ms pooling rate)
        key = cv2.waitKey(1)
        
        # Salir del programa
        if key == ord('q'):
            # Cancelar timer de captura
            try:
                timer2.cancel()
                print("Terminar Timer de Captura")
            except:
                pass

            print("Salir del Programa")
            break
        
        # Captura Manual
        elif key == ord('c'):
            # Cancelar timer de foto sincrónica
            try:
                timer2.cancel()
                print("Terminar Timer de Captura")
            except:
                pass

            print("Tomar Captura Manual", datetime.datetime.now())
            ctrl = depthai.CameraControl()
            ctrl.setCaptureStill(True)
            controlQueue.send(ctrl)

        # Ajuste Automático de Foco Continuo
        elif key == ord('f'):
            print("Autofocus enable, continuous")
            ctrl = depthai.CameraControl()
            ctrl.setAutoFocusMode(depthai.CameraControl.AutoFocusMode.CONTINUOUS_VIDEO)
            controlQueue.send(ctrl)
        
        # Gatillar Autofoco y deshabilitar Autofoco continuo
        elif key == ord('t'):
            print("Autofocus trigger (and disable continuous)")
            ctrl = depthai.CameraControl()
            ctrl.setAutoFocusMode(depthai.CameraControl.AutoFocusMode.AUTO)
            ctrl.setAutoFocusTrigger()
            controlQueue.send(ctrl)
        
        # Ajuste Manual de Foco
        elif key in [ord(','), ord('.')]:
            if key == ord(','): lensPos -= LENS_STEP
            if key == ord('.'): lensPos += LENS_STEP
            lensPos = clamp(lensPos, lensMin, lensMax)
            print("Setting manual focus, lens position: ", lensPos)
            ctrl = depthai.CameraControl()
            ctrl.setManualFocus(lensPos)
            controlQueue.send(ctrl)

        # Ajuste Automático de Tiempo de Exposición
        elif key == ord('e'):
            print("Autoexposure enable")
            ctrl = depthai.CameraControl()
            ctrl.setAutoExposureEnable()
            controlQueue.send(ctrl)
        
        # Ajuste Manual Tiempo de Exposición e ISO
        elif key in [ord('i'), ord('o'), ord('k'), ord('l')]:
            if key == ord('i'): expTime -= EXP_STEP
            if key == ord('o'): expTime += EXP_STEP
            if key == ord('k'): sensIso -= ISO_STEP
            if key == ord('l'): sensIso += ISO_STEP
            expTime = clamp(expTime, expMin, expMax)
            sensIso = clamp(sensIso, sensMin, sensMax)
            print("Setting manual exposure, time: ", expTime, "iso: ", sensIso)
            ctrl = depthai.CameraControl()
            ctrl.setManualExposure(expTime, sensIso)
            controlQueue.send(ctrl)
        
        # Ajuste Automático de Balance de Blanco
        elif key == ord('b'):
            print("Habilita balance de blanco automatico")
            ctrl = depthai.CameraControl()
            ctrl.setAutoWhiteBalanceMode(depthai.CameraControl.AutoWhiteBalanceMode.AUTO)
            controlQueue.send(ctrl)

        # Ajuste Manual de Balance de Blancos
        elif key in [ord('{'), ord('}')]:
            if key == ord('{'): wbManual -= WB_STEP
            if key == ord('}'): wbManual += WB_STEP
            wbManual = clamp(wbManual, wbMin, wbMax)
            print("Setting manual white balance, temperature: ", wbManual, "K")
            ctrl = depthai.CameraControl()
            ctrl.setManualWhiteBalance(wbManual)
            controlQueue.send(ctrl)