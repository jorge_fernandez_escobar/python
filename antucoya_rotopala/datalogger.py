import datetime
import time
import adafruit_dht
import board
import smbus
import subprocess
import os

# 1-Wire DHT 
dhtDevice = adafruit_dht.DHT22(board.D4, use_pulseio=False)
temp = dhtDevice.temperature
hum = dhtDevice.humidity

# I2C Lux
bus = smbus.SMBus(1)
 
bus.write_byte_data(0x39, 0x00 | 0x80, 0x03)
bus.write_byte_data(0x39, 0x01 | 0x80, 0x02)
 
time.sleep(0.5)
data0 = bus.read_i2c_block_data(0x39, 0x0C | 0x80, 2)
data1 = bus.read_i2c_block_data(0x39, 0x0E | 0x80, 2)

fullSpectrum = data0[1]*256 + data0[0]
IR = data1[1]*256 + data1[0]
visibleSpectrum = fullSpectrum - IR

# Fecha, hora y timestamp
ct = datetime.datetime.now()
Date = str(ct.date()) 
Time = ct.time().strftime("%H:%M:%S")
timestamp = str(int(ct.timestamp()))
#print(ct,Date,Time,timestamp)

imageName = Date + '_' + Time + '.jpg'
#print(imageFileName)

path = '/home/pi/Antucoya/' + Date
try:
    os.mkdir(path)
except OSError as error:
    pass#print(error)    

logName = path + '/' + Date + '.csv'
#print(logName)

# Escribir en log (append)
with open('%s' % logName, 'a') as f:
    print(timestamp,temp,hum,fullSpectrum,IR,visibleSpectrum,imageName,sep=',',file=f)


# Tomar imágen
cm = "libcamera-still -o"
bashCommand = cm + ' ' + path + '/' + imageName + " -n"
#print(bashCommand)
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)

