import time
import board
import adafruit_dht

# Initial the dht device, with data pin connected to:
# you can pass DHT22 use_pulseio=False if you wouldn't like to use pulseio.
# This may be necessary on a Linux single board computer like the Raspberry Pi
dhtDevice = adafruit_dht.DHT22(board.D4, use_pulseio=False)

temperature_c = dhtDevice.temperature
humidity = dhtDevice.humidity

print("Temperatura: {:.1f} °C  Humedad: {}% ".format(temperature_c, humidity))
